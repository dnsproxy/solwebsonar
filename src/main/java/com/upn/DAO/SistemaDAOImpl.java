package com.upn.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.upn.Models.Alumno;
import com.upn.Models.ConsolidadoMatricula;
import com.upn.Models.Departamento;
import com.upn.Models.Distrito;
import com.upn.Models.Grado;
import com.upn.Models.Nivel;
import com.upn.Models.Provincia;

public class SistemaDAOImpl implements SistemaDAO {

	Connection connection = null;
	Statement statement = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	
	public String Login(boolean pool, String email, String pass) {
		
		String var = "";
		
		try {
			
			InitialContext initialContext = new InitialContext();
			DataSource dataSource = (DataSource)initialContext.lookup("java:/SQLiteDS");
			Connection connection = dataSource.getConnection();
			statement = connection.createStatement();
												
			resultSet = statement.executeQuery("SELECT * FROM Usuario WHERE Email = "+"'"+email+"'"+"AND Password ="+"'"+pass+"'");
	
			if(resultSet.next()) {
				var = "Y";
			}else {
				var = "N";
			}
			
			resultSet.close();
			statement.close();
			connection.close();
			
			} catch (NamingException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();			
			}
		
		return var;
						
	}
	
	@Override
	public List<Departamento> ReadDepartamento(boolean pool) {
		
		List<Departamento> listDepartamento= new ArrayList<Departamento>();
		
		try {			
							
			InitialContext initialContext = new InitialContext();
			DataSource dataSource = (DataSource)initialContext.lookup("java:/SQLiteDS");
			Connection connection = dataSource.getConnection();
			statement = connection.createStatement();
			
			resultSet = statement.executeQuery("SELECT DISTINCT * FROM Departamento ORDER BY Nombre");
			
			while (resultSet.next()) {
				Departamento d = new Departamento();
				d.setDepartamentoId(resultSet.getInt(1));
				d.setNombre(resultSet.getString(2));
				listDepartamento.add(d);
        }

		resultSet.close();
		statement.close();
		connection.close();
		
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();			
		}
					
	return listDepartamento;
	}
	
	public List<Provincia> ReadProvincia(boolean pool) {
		
		List<Provincia> listProvincia= new ArrayList<Provincia>();
		
		try {			
							
			InitialContext initialContext = new InitialContext();
			DataSource dataSource = (DataSource)initialContext.lookup("java:/SQLiteDS");
			Connection connection = dataSource.getConnection();
			statement = connection.createStatement();
			
			resultSet = statement.executeQuery("SELECT DISTINCT * FROM Provincia ORDER BY Nombre");
			
			while (resultSet.next()) {
				Provincia p = new Provincia();
				p.setProvinciaId(resultSet.getInt(1));
				p.setNombre(resultSet.getString(2));
				p.setDepartamentoId(resultSet.getInt(3));
				listProvincia.add(p);
        }

		resultSet.close();
		statement.close();
		connection.close();
		
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();			
		}
					
	return listProvincia;
	}
	
	public List<Distrito> ReadDistrito(boolean pool) {
		List<Distrito> listDistrito= new ArrayList<Distrito>();
		try {			
			
			InitialContext initialContext = new InitialContext();
			DataSource dataSource = (DataSource)initialContext.lookup("java:/SQLiteDS");
			Connection connection = dataSource.getConnection();
			statement = connection.createStatement();
			
			resultSet = statement.executeQuery("SELECT DISTINCT * FROM Distrito ORDER BY Nombre");
			
			while (resultSet.next()) {
				Distrito d = new Distrito();
				d.setDistritoId(resultSet.getInt(1));
				d.setNombre(resultSet.getString(2));
				d.setProvinciaId(resultSet.getInt(3));
				listDistrito.add(d);
        }

		resultSet.close();
		statement.close();
		connection.close();
		
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();			
		}
					
	return listDistrito;
	}

	@Override
	public List<Nivel> ReadNivel(boolean pool) {
			
		List<Nivel> listNivel= new ArrayList<Nivel>();
			
			try {			
								
				InitialContext initialContext = new InitialContext();
				DataSource dataSource = (DataSource)initialContext.lookup("java:/SQLiteDS");
				Connection connection = dataSource.getConnection();
				statement = connection.createStatement();
				
				resultSet = statement.executeQuery("SELECT DISTINCT * FROM Nivel ORDER BY Nombre");
				
				while (resultSet.next()) {
					Nivel n = new Nivel();
					n.setNivelId(resultSet.getInt(1));
					n.setNombre(resultSet.getString(2));
					listNivel.add(n);
				}

			resultSet.close();
			statement.close();
			connection.close();
			
			} catch (NamingException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();			
			}
						
		return listNivel;

	}

	@Override
	public List<Grado> ReadGrado(boolean pool) {
		
		List<Grado> listGrado= new ArrayList<Grado>();
		
		try {			
							
			InitialContext initialContext = new InitialContext();
			DataSource dataSource = (DataSource)initialContext.lookup("java:/SQLiteDS");
			Connection connection = dataSource.getConnection();
			statement = connection.createStatement();
			
			resultSet = statement.executeQuery("SELECT DISTINCT * FROM Grado ORDER BY Nombre");
			
			while (resultSet.next()) {
				Grado g = new Grado();
				g.setGradoId(resultSet.getInt(1));
				g.setNombre(resultSet.getString(2));
				g.setNivelId(resultSet.getInt(3));
				listGrado.add(g);
			}

		resultSet.close();
		statement.close();
		connection.close();
		
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();			
		}
					
	return listGrado;
	}

	//----- CRUD CONSOLIDADO -----//
	//CREATE
	@Override
	public void CreateConsolidadoMatricula(boolean pool, ConsolidadoMatricula consolidadoMatricula) {	
		
		try {
			
			InitialContext initialContext = new InitialContext();
			DataSource dataSource = (DataSource)initialContext.lookup("java:/SQLiteDS");
			Connection connection = dataSource.getConnection();
			statement = connection.createStatement();
			
				preparedStatement=connection.prepareStatement("INSERT INTO ConsolidadoMatricula(AnioAcademico, NivelId, GradoId) VALUES (?,?,?)");
				preparedStatement.setString(1, consolidadoMatricula.getAnioAcademico());
				preparedStatement.setInt(2,consolidadoMatricula.getNivelId());
				preparedStatement.setInt(3,consolidadoMatricula.getGradoId());
				preparedStatement.executeUpdate();	

			statement.close();
			connection.close();
			
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();			
		}
		
	}
	//READ
	@Override
	public List<ConsolidadoMatricula> ReadConsolidadoMatricula(boolean pool) {
		
		List<ConsolidadoMatricula> listConsolidadoMatricula= new ArrayList<ConsolidadoMatricula>();
		
		try {			
							
			InitialContext initialContext = new InitialContext();
			DataSource dataSource = (DataSource)initialContext.lookup("java:/SQLiteDS");
			Connection connection = dataSource.getConnection();
			statement = connection.createStatement();
			
			resultSet = statement.executeQuery("SELECT DISTINCT ConsolidadoMatriculaId, AnioAcademico, N.NivelId, N.Nombre, G.GradoId, G.Nombre FROM ConsolidadoMatricula AS C, Nivel AS N, Grado AS G INNER JOIN Nivel ON N.NivelId = C.NivelId INNER JOIN Grado ON G.GradoId = C.GradoId ORDER BY AnioAcademico");
			
			while (resultSet.next()) {
				ConsolidadoMatricula c = new ConsolidadoMatricula();
				c.setConsolidadoMatriculaId(resultSet.getInt(1));
				c.setAnioAcademico(resultSet.getString(2));
				c.setNivelId(resultSet.getInt(3));
				c.setNivelNombre(resultSet.getString(4));
				c.setGradoId(resultSet.getInt(5));
				c.setGradoNombre(resultSet.getString(6));
				listConsolidadoMatricula.add(c);
			}

		resultSet.close();
		statement.close();
		connection.close();
		
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();			
		}
					
		return listConsolidadoMatricula;
	}
	//UPDATE
	@Override
	public void UpdateConsolidadoMatricula(boolean pool, ConsolidadoMatricula consolidadoMatricula) {
			
		try {
			
			InitialContext initialContext = new InitialContext();
			DataSource dataSource = (DataSource)initialContext.lookup("java:/SQLiteDS");
			Connection connection = dataSource.getConnection();
			statement = connection.createStatement();
			
			preparedStatement=connection.prepareStatement("UPDATE ConsolidadoMatricula SET AnioAcademico='"+consolidadoMatricula.getAnioAcademico()+"', NivelId='"+consolidadoMatricula.getNivelId()+"', GradoId='"+consolidadoMatricula.getGradoId()+"' WHERE ConsolidadoMatriculaId=?");
			preparedStatement.setInt(1, consolidadoMatricula.getConsolidadoMatriculaId());
			preparedStatement.executeUpdate();
			
			statement.close();
			connection.close();
			
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();			
		}	

	}
	//DELETE
	@Override
	public void DeleteConsolidadoMatricula(boolean pool, int consolidadoMatriculaId) {
		
		try {
			
			InitialContext initialContext = new InitialContext();
			DataSource dataSource = (DataSource)initialContext.lookup("java:/SQLiteDS");
			Connection connection = dataSource.getConnection();
			statement = connection.createStatement();
												
			preparedStatement=connection.prepareStatement("DELETE FROM ConsolidadoMatricula WHERE consolidadoMatriculaId=?");
			preparedStatement.setInt(1, consolidadoMatriculaId);			
			preparedStatement.executeUpdate();
			
			statement.close();
			connection.close();
			
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();			
		}
		
	}
	//DETAILS
	@Override
	public List<ConsolidadoMatricula> ReadConsolidadoMatriculaById(boolean pool, int consolidadoMatriculaId){
	  
	      List<ConsolidadoMatricula> listConsolidadoMatricula= new ArrayList<ConsolidadoMatricula>();
	      
	      try {
	        
	        InitialContext initialContext = new InitialContext();
	        DataSource dataSource = (DataSource)initialContext.lookup("java:/SQLiteDS");
	        Connection connection = dataSource.getConnection();
	        statement = connection.createStatement();
	        
	        resultSet = statement.executeQuery("SELECT DISTINCT ConsolidadoMatriculaId, AnioAcademico, N.NivelId, N.Nombre, G.GradoId, G.Nombre FROM ConsolidadoMatricula AS C, Nivel AS N, Grado AS G INNER JOIN Nivel ON N.NivelId = C.NivelId INNER JOIN Grado ON G.GradoId = C.GradoId WHERE ConsolidadoMatriculaId="+consolidadoMatriculaId);
	        
			ConsolidadoMatricula c = new ConsolidadoMatricula();
			c.setConsolidadoMatriculaId(resultSet.getInt(1));
			c.setAnioAcademico(resultSet.getString(2));
			c.setNivelId(resultSet.getInt(3));
			c.setNivelNombre(resultSet.getString(4));
			c.setGradoId(resultSet.getInt(5));
			c.setGradoNombre(resultSet.getString(6));
			listConsolidadoMatricula.add(c);
	        
	        resultSet.close();
	        statement.close();
	        connection.close();
	        
	      } catch (NamingException e) {
	        e.printStackTrace();
	      } catch (SQLException e) {
	        e.printStackTrace();      
	      }                   
	    return listConsolidadoMatricula;
	  }
	
	//----- CRUD CONSOLIDADO -----//
	//CREATE
	@Override
	public void CreateAlumno(boolean pool, Alumno alumno) {	
		
		try {
			
			InitialContext initialContext = new InitialContext();
			DataSource dataSource = (DataSource)initialContext.lookup("java:/SQLiteDS");
			Connection connection = dataSource.getConnection();
			statement = connection.createStatement();
			
				preparedStatement=connection.prepareStatement("INSERT INTO Alumno(ApellidoPaterno, ApellidoMaterno, Nombre, FechaNacimiento, Dni, Sexo, "
						+ "Foto, PartidaNacimiento, Direccion, Estado, FichaMatriculaSiagie, CertificadoEstudios, DepartamentoId, ProvinciaId, DistritoId, "
						+ "ConsolidadoMatriculaId) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				preparedStatement.setString(1, alumno.getApellidoPaterno());
				preparedStatement.setString(2, alumno.getApellidoMaterno());
				preparedStatement.setString(3, alumno.getNombre());
				preparedStatement.setString(4, alumno.getFechaNacimiento());
				preparedStatement.setString(5, alumno.getDNI());
				preparedStatement.setString(6, alumno.getSexo());
				preparedStatement.setString(7, alumno.getFoto());
				preparedStatement.setString(8, alumno.getPartidaNacimiento());
				preparedStatement.setString(9, alumno.getDireccion());
				preparedStatement.setString(10, alumno.getEstado());
				preparedStatement.setString(11, alumno.getFichaMatriculaSiagie());
				preparedStatement.setString(12, alumno.getCertificadoEstudios());
				preparedStatement.setInt(13, alumno.getDepartamentoId());
				preparedStatement.setInt(14, alumno.getProvinciaId());
				preparedStatement.setInt(15, alumno.getDistritoId());
				preparedStatement.setInt(16, alumno.getConsolidadoMatriculaId());
				preparedStatement.executeUpdate();	

			statement.close();
			connection.close();
			
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();			
		}
		
	}
	//READ
	public List<Alumno> ReadAlumno(boolean pool){
		List<Alumno> listAlumno= new ArrayList<Alumno>();
		
		try {			
							
			InitialContext initialContext = new InitialContext();
			DataSource dataSource = (DataSource)initialContext.lookup("java:/SQLiteDS");
			Connection connection = dataSource.getConnection();
			statement = connection.createStatement();

			resultSet = statement.executeQuery("SELECT DISTINCT AlumnoId, ApellidoPaterno, ApellidoMaterno, A.Nombre, FechaNacimiento, "
					+ "Dni, Sexo, Foto, PartidaNacimiento, Direccion, Estado, FichaMatriculaSiagie, CertificadoEstudios, D.DepartamentoId, "
					+ "D.Nombre, P.ProvinciaId, P.Nombre, T.DistritoId, T.Nombre, C.ConsolidadoMatriculaId, C.AnioAcademico FROM Alumno AS A, "
					+ "Departamento AS D, Provincia AS P, Distrito AS T, ConsolidadoMatricula AS C INNER JOIN Departamento ON "
					+ "D.DepartamentoId = A.DepartamentoId INNER JOIN Provincia ON P.ProvinciaId = A.ProvinciaId INNER JOIN Distrito ON "
					+ "T.DistritoId = A.DistritoId INNER JOIN ConsolidadoMatricula ON C.ConsolidadoMatriculaId = A.ConsolidadoMatriculaId "
					+ "ORDER BY ApellidoPaterno");
					
			while (resultSet.next()) {
				Alumno a = new Alumno();
				a.setAlumnoId(resultSet.getInt(1));
				a.setApellidoPaterno(resultSet.getString(2));
				a.setApellidoMaterno(resultSet.getString(3));
				a.setNombre(resultSet.getString(4));
				a.setFechaNacimiento(resultSet.getString(5));
				a.setDNI(resultSet.getString(6));
				a.setSexo(resultSet.getString(7));
				a.setFoto(resultSet.getString(8));
				a.setPartidaNacimiento(resultSet.getString(9));
				a.setDireccion(resultSet.getString(10));
				a.setEstado(resultSet.getString(11));
				a.setFichaMatriculaSiagie(resultSet.getString(12));
				a.setCertificadoEstudios(resultSet.getString(13));
				a.setDepartamentoId(resultSet.getInt(14));
				a.setDepartamentoNombre(resultSet.getString(15));
				a.setProvinciaId(resultSet.getInt(16));
				a.setProvinciaNombre(resultSet.getString(17));
				a.setDistritoId(resultSet.getInt(18));
				a.setDistritoNombre(resultSet.getString(19));
				a.setConsolidadoMatriculaId(resultSet.getInt(20));
				a.setConsolidadoMatriculaAnioAcademico(resultSet.getString(21));
				listAlumno.add(a);
			}

		resultSet.close();
		statement.close();
		connection.close();
		
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();			
		}
					
		return listAlumno;
	}
	//UPDATE
	//DELETE
	//DETAILS

}
