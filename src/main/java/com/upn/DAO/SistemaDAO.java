package com.upn.DAO;

import java.util.List;

import com.upn.Models.Alumno;
import com.upn.Models.ConsolidadoMatricula;
import com.upn.Models.Departamento;
import com.upn.Models.Distrito;
import com.upn.Models.Grado;
import com.upn.Models.Nivel;
import com.upn.Models.Provincia;

public interface SistemaDAO {

	public String Login(boolean pool, String email, String pass);
	
	//READ DEPART
	public List<Departamento> ReadDepartamento(boolean pool);
	
	//READ PROV
	public List<Provincia> ReadProvincia(boolean pool);
	
	//READ DIST
	public List<Distrito> ReadDistrito(boolean pool);
	
	//READ NIV
	public List<Nivel> ReadNivel(boolean pool);
	
	//READ GRADO
	public List<Grado> ReadGrado(boolean pool);
	
	//CRUD CONSOLIDADO
	public void CreateConsolidadoMatricula(boolean pool, ConsolidadoMatricula consolidadoMatricula);
	public List<ConsolidadoMatricula> ReadConsolidadoMatricula(boolean pool);
	public void UpdateConsolidadoMatricula(boolean pool, ConsolidadoMatricula consolidadoMatricula);
	public void DeleteConsolidadoMatricula(boolean pool, int consolidadoMatriculaId);
	public List<ConsolidadoMatricula> ReadConsolidadoMatriculaById(boolean pool, int consolidadoMatriculaId);
	
	//CRUD ALUMNO
	public void CreateAlumno(boolean pool, Alumno alumno);
	public List<Alumno> ReadAlumno(boolean pool);
}