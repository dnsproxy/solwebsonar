package com.upn.Controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import com.upn.DAO.SistemaDAO;
import com.upn.DAO.SistemaDAOImpl;
import com.upn.Models.Alumno;
import com.upn.Models.ConsolidadoMatricula;
import com.upn.Models.Departamento;
import com.upn.Models.Distrito;
import com.upn.Models.Grado;
import com.upn.Models.Nivel;
import com.upn.Models.Provincia;

@Controller
public class HomeController {
	
	SistemaDAO dao = new SistemaDAOImpl();

	@RequestMapping(value="/")
	public ModelAndView home(ModelAndView model){		
	    model.setViewName("home");	 
	    return model;
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView login(@RequestParam ("email") String email, @RequestParam ("pass") String pass, ModelAndView model) {		
		String v = dao.Login(true, email, pass);	
		if (v == "Y") {	
			model.addObject("usuario",email);
			model.setViewName("dashboard");
		    return model;
		}else {
			model.setViewName("home");	 
		    return model;
		}			
	}
	
	@RequestMapping(value="/nuevamatri")
	public ModelAndView nuevamatri(ModelAndView model){		
		
		List<Departamento> departamentos = new ArrayList<Departamento>();
		departamentos = dao.ReadDepartamento(true);
		model.addObject("listDepartamento", departamentos);	
		
		List<Provincia> provincias = new ArrayList<Provincia>();
		provincias = dao.ReadProvincia(true);
		model.addObject("listProvincia", provincias);	
		
		List<Distrito> distritos = new ArrayList<Distrito>();
		distritos = dao.ReadDistrito(true);
		model.addObject("listDistrito", distritos);	
		
	    model.setViewName("nuevamatri");	 
	    return model;
	}
	
	//CRUD CONSOLIDADO
	//--
	@RequestMapping(value="/indexconsolidado")
	public ModelAndView indexconsolidado(ModelAndView model){
		
		List<ConsolidadoMatricula> consolidadoMatriculas = new ArrayList<ConsolidadoMatricula>();
		consolidadoMatriculas = dao.ReadConsolidadoMatricula(true);
		//LISTAR NIVELES
		List<Nivel> nivels = new ArrayList<Nivel>();
		nivels = dao.ReadNivel(true);
		model.addObject("listNivel", nivels);
		//LISTAR GRADOS
		List<Grado> grados = new ArrayList<Grado>();
		grados = dao.ReadGrado(true);
		model.addObject("listGrado", grados);
 		model.setViewName("createconsolidado");
		model.addObject("listConsolidadoMatricula", consolidadoMatriculas);	
	    model.setViewName("indexconsolidado");	 
	    return model;
	}
	
	@RequestMapping(value = "/createconsolidado", method = RequestMethod.GET)
	public ModelAndView createconsolidado(ModelAndView model) {
		//LISTAR NIVELES
		List<Nivel> nivels = new ArrayList<Nivel>();
		nivels = dao.ReadNivel(true);
		model.addObject("listNivel", nivels);
		//LISTAR GRADOS
		List<Grado> grados = new ArrayList<Grado>();
		grados = dao.ReadGrado(true);
		model.addObject("listGrado", grados);
 		model.setViewName("createconsolidado");
	    return model;
	}
	
	@RequestMapping(value = "/saveconsolidado", method = RequestMethod.POST)
	public ModelAndView saveconsolidado(ModelAndView model, @ModelAttribute ConsolidadoMatricula consolidadoMatricula) {			
		dao.CreateConsolidadoMatricula(true, consolidadoMatricula);
		return new ModelAndView("redirect:/indexconsolidado");
	}
	
	@RequestMapping(value = "/deleteconsolidado/{id}")
	public ModelAndView deleteconsolidado(@ModelAttribute ConsolidadoMatricula consolidadoMatricula, @PathVariable("id") int id) {	
		dao.DeleteConsolidadoMatricula(true, id);
		return new ModelAndView("redirect:/indexconsolidado");
	}
	
	@RequestMapping(value = "/buscarconsolidado/{id}", method = RequestMethod.GET)
	public ModelAndView buscarconsolidado(ModelAndView model, @PathVariable("id") int id) {		
		
		//LISTAR CONSOLIDADOS
		List<ConsolidadoMatricula> consolidadoMatriculas = new ArrayList<ConsolidadoMatricula>();
		consolidadoMatriculas = dao.ReadConsolidadoMatriculaById(true, id);
		model.addObject("listConsolidadoMatricula", consolidadoMatriculas);	
		
		//LISTAR NIVELES
		List<Nivel> nivels = new ArrayList<Nivel>();
		nivels = dao.ReadNivel(true);
		model.addObject("listNivel", nivels);
		
		//LISTAR GRADOS
		List<Grado> grados = new ArrayList<Grado>();
		grados = dao.ReadGrado(true);
		model.addObject("listGrado", grados);
	    
 		model.setViewName("buscarconsolidado");
	    return model;
	}
	
	@RequestMapping(value = "/actualizarconsolidado", method = RequestMethod.POST)
	public ModelAndView actualizarconsolidado(ModelAndView model, @ModelAttribute ConsolidadoMatricula consolidadoMatricula) {		
		dao.UpdateConsolidadoMatricula(true, consolidadoMatricula);
		return new ModelAndView("redirect:/indexconsolidado");
	}
	//--
	
	//CRUD ALUMNO
	//--
	@RequestMapping(value="/indexalumno")
	public ModelAndView indexalumno(ModelAndView model){
		
		List<Alumno> alumnos = new ArrayList<Alumno>();
		alumnos = dao.ReadAlumno(true);
		model.addObject("listAlumno", alumnos);	
		
	    model.setViewName("indexalumno");	 
	    return model;
	}
	
	@RequestMapping(value = "/createalumno", method = RequestMethod.GET)
	public ModelAndView createalumno(ModelAndView model) {
		
		//LISTAR DEPARTAMENTOS
		List<Departamento> departamentos = new ArrayList<Departamento>();
		departamentos = dao.ReadDepartamento(true);
		model.addObject("listDepartamento", departamentos);
		//LISTAR PROVINCIAS
		//LISTAR DISTRITOS		
		//LISTAR CONSOLIDADOS
		/*List<Grado> grados = new ArrayList<Grado>();
		grados = dao.ReadGrado(true);
		model.addObject("listGrado", grados);*/
		
		
 		model.setViewName("createalumno");
	    return model;
	}
	//--
}
