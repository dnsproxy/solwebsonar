package com.upn.Models;

public class Alumno {

	private int AlumnoId;
	private String Nombre;
	private String ApellidoPaterno;
	private String ApellidoMaterno;
	private String FechaNacimiento;
	private String DNI;
	private String Sexo;
	private String Foto;
	private String PartidaNacimiento;
	private String Direccion;
	private String Estado;
	private String FichaMatriculaSiagie;
	private String CertificadoEstudios;
	private int DepartamentoId;
	private String DepartamentoNombre;
	private int ProvinciaId;
	private String ProvinciaNombre;
	private int DistritoId;
	private String DistritoNombre;
	private int ConsolidadoMatriculaId;
	private String ConsolidadoMatriculaAnioAcademico;
	
	public Alumno() {
		super();
	}

	public Alumno(int alumnoId, String nombre, String apellidoPaterno, String apellidoMaterno, String fechaNacimiento,
			String dNI, String sexo, String foto, String partidaNacimiento, String direccion, String estado,
			String fichaMatriculaSiagie, String certificadoEstudios, int departamentoId, String departamentoNombre,
			int provinciaId, String provinciaNombre, int distritoId, String distritoNombre, int consolidadoMatriculaId,
			String consolidadoMatriculaAnioAcademico) {
		super();
		AlumnoId = alumnoId;
		Nombre = nombre;
		ApellidoPaterno = apellidoPaterno;
		ApellidoMaterno = apellidoMaterno;
		FechaNacimiento = fechaNacimiento;
		DNI = dNI;
		Sexo = sexo;
		Foto = foto;
		PartidaNacimiento = partidaNacimiento;
		Direccion = direccion;
		Estado = estado;
		FichaMatriculaSiagie = fichaMatriculaSiagie;
		CertificadoEstudios = certificadoEstudios;
		DepartamentoId = departamentoId;
		DepartamentoNombre = departamentoNombre;
		ProvinciaId = provinciaId;
		ProvinciaNombre = provinciaNombre;
		DistritoId = distritoId;
		DistritoNombre = distritoNombre;
		ConsolidadoMatriculaId = consolidadoMatriculaId;
		ConsolidadoMatriculaAnioAcademico = consolidadoMatriculaAnioAcademico;
	}

	public int getAlumnoId() {
		return AlumnoId;
	}

	public void setAlumnoId(int alumnoId) {
		AlumnoId = alumnoId;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public String getApellidoPaterno() {
		return ApellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		ApellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return ApellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		ApellidoMaterno = apellidoMaterno;
	}

	public String getFechaNacimiento() {
		return FechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		FechaNacimiento = fechaNacimiento;
	}

	public String getDNI() {
		return DNI;
	}

	public void setDNI(String dNI) {
		DNI = dNI;
	}

	public String getSexo() {
		return Sexo;
	}

	public void setSexo(String sexo) {
		Sexo = sexo;
	}

	public String getFoto() {
		return Foto;
	}

	public void setFoto(String foto) {
		Foto = foto;
	}

	public String getPartidaNacimiento() {
		return PartidaNacimiento;
	}

	public void setPartidaNacimiento(String partidaNacimiento) {
		PartidaNacimiento = partidaNacimiento;
	}

	public String getDireccion() {
		return Direccion;
	}

	public void setDireccion(String direccion) {
		Direccion = direccion;
	}

	public String getEstado() {
		return Estado;
	}

	public void setEstado(String estado) {
		Estado = estado;
	}

	public String getFichaMatriculaSiagie() {
		return FichaMatriculaSiagie;
	}

	public void setFichaMatriculaSiagie(String fichaMatriculaSiagie) {
		FichaMatriculaSiagie = fichaMatriculaSiagie;
	}

	public String getCertificadoEstudios() {
		return CertificadoEstudios;
	}

	public void setCertificadoEstudios(String certificadoEstudios) {
		CertificadoEstudios = certificadoEstudios;
	}

	public int getDepartamentoId() {
		return DepartamentoId;
	}

	public void setDepartamentoId(int departamentoId) {
		DepartamentoId = departamentoId;
	}

	public String getDepartamentoNombre() {
		return DepartamentoNombre;
	}

	public void setDepartamentoNombre(String departamentoNombre) {
		DepartamentoNombre = departamentoNombre;
	}

	public int getProvinciaId() {
		return ProvinciaId;
	}

	public void setProvinciaId(int provinciaId) {
		ProvinciaId = provinciaId;
	}

	public String getProvinciaNombre() {
		return ProvinciaNombre;
	}

	public void setProvinciaNombre(String provinciaNombre) {
		ProvinciaNombre = provinciaNombre;
	}

	public int getDistritoId() {
		return DistritoId;
	}

	public void setDistritoId(int distritoId) {
		DistritoId = distritoId;
	}

	public String getDistritoNombre() {
		return DistritoNombre;
	}

	public void setDistritoNombre(String distritoNombre) {
		DistritoNombre = distritoNombre;
	}

	public int getConsolidadoMatriculaId() {
		return ConsolidadoMatriculaId;
	}

	public void setConsolidadoMatriculaId(int consolidadoMatriculaId) {
		ConsolidadoMatriculaId = consolidadoMatriculaId;
	}

	public String getConsolidadoMatriculaAnioAcademico() {
		return ConsolidadoMatriculaAnioAcademico;
	}

	public void setConsolidadoMatriculaAnioAcademico(String consolidadoMatriculaAnioAcademico) {
		ConsolidadoMatriculaAnioAcademico = consolidadoMatriculaAnioAcademico;
	}
	
}
