package com.upn.Models;

public class Provincia {

	private int ProvinciaId;
	private String Nombre;
	private int DepartamentoId;
	
	public Provincia() {
		super();
	}
	public Provincia(int provinciaId, String nombre, int departamentoId) {
		super();
		ProvinciaId = provinciaId;
		Nombre = nombre;
		setDepartamentoId(departamentoId);
	}
	public int getProvinciaId() {
		return ProvinciaId;
	}
	public void setProvinciaId(int provinciaId) {
		ProvinciaId = provinciaId;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public int getDepartamentoId() {
		return DepartamentoId;
	}
	public void setDepartamentoId(int departamentoId) {
		DepartamentoId = departamentoId;
	}
	
}
