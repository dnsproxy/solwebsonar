package com.upn.Models;

public class Usuario {

	private int UsuarioId;
    private String Email;
    private String Password;
    private int CargoId;
    
	public Usuario() {
		super();
	}

	public Usuario(int usuarioId, String email, String password, int cargoId) {
		super();
		UsuarioId = usuarioId;
		Email = email;
		Password = password;
		CargoId = cargoId;
	}

	public int getUsuarioId() {
		return UsuarioId;
	}

	public void setUsuarioId(int usuarioId) {
		UsuarioId = usuarioId;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public int getCargoId() {
		return CargoId;
	}

	public void setCargoId(int cargoId) {
		CargoId = cargoId;
	}
    
}
