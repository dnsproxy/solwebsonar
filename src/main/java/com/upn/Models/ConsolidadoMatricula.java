package com.upn.Models;

public class ConsolidadoMatricula {

	private int ConsolidadoMatriculaId;
	private String AnioAcademico;
	private int NivelId;
	private String NivelNombre;
	private int GradoId;
	private String GradoNombre;
	
	public ConsolidadoMatricula() {
		super();
	}

	public ConsolidadoMatricula(int consolidadoMatriculaId, String anioAcademico, int nivelId, String nivelNombre,
			int gradoId, String gradoNombre) {
		super();
		ConsolidadoMatriculaId = consolidadoMatriculaId;
		AnioAcademico = anioAcademico;
		NivelId = nivelId;
		NivelNombre = nivelNombre;
		GradoId = gradoId;
		GradoNombre = gradoNombre;
	}

	public int getConsolidadoMatriculaId() {
		return ConsolidadoMatriculaId;
	}

	public void setConsolidadoMatriculaId(int consolidadoMatriculaId) {
		ConsolidadoMatriculaId = consolidadoMatriculaId;
	}

	public String getAnioAcademico() {
		return AnioAcademico;
	}

	public void setAnioAcademico(String anioAcademico) {
		AnioAcademico = anioAcademico;
	}

	public int getNivelId() {
		return NivelId;
	}

	public void setNivelId(int nivelId) {
		NivelId = nivelId;
	}

	public String getNivelNombre() {
		return NivelNombre;
	}

	public void setNivelNombre(String nivelNombre) {
		NivelNombre = nivelNombre;
	}

	public int getGradoId() {
		return GradoId;
	}

	public void setGradoId(int gradoId) {
		GradoId = gradoId;
	}

	public String getGradoNombre() {
		return GradoNombre;
	}

	public void setGradoNombre(String gradoNombre) {
		GradoNombre = gradoNombre;
	}
	
}
