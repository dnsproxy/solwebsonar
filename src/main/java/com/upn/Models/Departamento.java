package com.upn.Models;

public class Departamento {

	private int DepartamentoId;
	private String Nombre;
	
	public Departamento() {
		super();
	}
	public Departamento(int departamentoId, String nombre) {
		super();
		DepartamentoId = departamentoId;
		Nombre = nombre;
	}
	public int getDepartamentoId() {
		return DepartamentoId;
	}
	public void setDepartamentoId(int departamentoId) {
		DepartamentoId = departamentoId;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	
}
