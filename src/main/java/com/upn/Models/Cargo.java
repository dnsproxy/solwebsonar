package com.upn.Models;

public class Cargo {

	private int CargoId;
	private String Descripcion;
	
	public Cargo() {
		super();
	}

	public Cargo(int cargoId, String descripcion) {
		super();
		CargoId = cargoId;
		Descripcion = descripcion;
	}

	public int getCargoId() {
		return CargoId;
	}

	public void setCargoId(int cargoId) {
		CargoId = cargoId;
	}

	public String getDescripcion() {
		return Descripcion;
	}

	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	
}
