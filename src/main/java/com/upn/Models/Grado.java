package com.upn.Models;

public class Grado {

	private int GradoId;
	private String Nombre;	
	private int NivelId;
	
	public Grado() {
		super();
	}

	public Grado(int gradoId, String nombre, int nivelId) {
		super();
		GradoId = gradoId;
		Nombre = nombre;
		NivelId = nivelId;
	}

	public int getGradoId() {
		return GradoId;
	}

	public void setGradoId(int gradoId) {
		GradoId = gradoId;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public int getNivelId() {
		return NivelId;
	}

	public void setNivelId(int nivelId) {
		NivelId = nivelId;
	}
	
}
