package com.upn.Models;

public class Distrito {

	private int DistritoId;
	private String Nombre;
	private int ProvinciaId;
	
	public Distrito() {
		super();
	}
	public Distrito(int distritoId, String nombre, int provinciaId) {
		super();
		DistritoId = distritoId;
		Nombre = nombre;
		ProvinciaId = provinciaId;
	}
	public int getDistritoId() {
		return DistritoId;
	}
	public void setDistritoId(int distritoId) {
		DistritoId = distritoId;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public int getProvinciaId() {
		return ProvinciaId;
	}
	public void setProvinciaId(int provinciaId) {
		ProvinciaId = provinciaId;
	}
	
}
