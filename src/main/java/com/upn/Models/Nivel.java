package com.upn.Models;

public class Nivel {

	private int NivelId;
	private String Nombre;
	
	public Nivel() {
		super();
	}
	public Nivel(int nivelId, String nombre) {
		super();
		NivelId = nivelId;
		Nombre = nombre;
	}
	public int getNivelId() {
		return NivelId;
	}
	public void setNivelId(int nivelId) {
		NivelId = nivelId;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	
	
}
