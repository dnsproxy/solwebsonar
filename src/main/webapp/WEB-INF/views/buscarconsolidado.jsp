
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
	<title>Actualizar Consolidado</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>

</head>
<body>
	<h1>Actualizar Consolidado</h1>
<c:set value="${listConsolidadoMatricula}" var="hola"/>
<c:forEach items="${listConsolidadoMatricula}" var="item">
	<form action="/PROYSOLWEB/actualizarconsolidado" method="post"> 
	<div class="form-group" style="display: none;">
    	<label for="consolidadoMatriculaId">Id</label>
    	<input type="number" class="form-control" name="consolidadoMatriculaId" id="consolidadoMatriculaId" value="${item.consolidadoMatriculaId}" required>
  	</div>
  	<div class="form-group">
    	<label for="">A�o Academico</label>
		<input class="date-own form-control" type="text" name="anioAcademico" id="anioAcademico" value="${item.anioAcademico}" required>
  	</div>
  	  	
  	<div class="form-group">
    	<label for="">Nivel</label>
    	<select name="nivelId" id="nivelId">
    		<option value="${item.nivelId}" selected disabled hidden>${item.nivelNombre}</option>
    		<c:forEach items="${listNivel}" var="item2">
  			<option value="${item2.nivelId}">${item2.nombre}</option>
  			</c:forEach>
		</select>
  	</div>
  	
  	<div class="form-group">
    	<label for="">Grado</label>
    	<select name="gradoId" id="gradoId">
    		<option value="${item.gradoId}" selected disabled hidden>${item.gradoNombre}</option>
    		<c:forEach items="${listGrado}" var="item2">
  			<option value="${item2.gradoId}">${item2.nombre}</option>
  			</c:forEach>
		</select>
  	</div>
	
  	<button type="submit" class="btn btn-primary">Crear</button>
  	
	</form>
</c:forEach>
</body>

  <script type="text/javascript">
      $('.date-own').datepicker({
         minViewMode: 2,
         format: 'yyyy'
       });
  </script>

</html>