<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
<head>
	<title>Alumnos</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<style>
	#var {
  		float: right;
  		clear: both;
	}
	</style>
</head>
<body>
<h2>Alumnos</h2>
<hr>
<div class="alert alert-primary" role="alert">
  <a class="btn btn-primary" href="#" role="button">Registrar Nuevo Alumno</a>
  <div id="var">Buscar: <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Buscar por apellido paterno..." title="Ingresa el apelliedo paterno del alumno"> </div>
</div>
<br>
<table class="table" id="myTable">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Apellidos</th>
      <th scope="col">Nombre</th>
      <th scope="col">Fecha Nacimiento</th>
      <th scope="col">DNI</th>
      <th scope="col">Sexo</th>
      <th scope="col">Foto</th>
      <th scope="col">Partida de Nacimiento</th>
      <th scope="col">Direccion</th>
      <th scope="col">Estado</th>
      <th scope="col">FichaSiagie</th>
      <th scope="col">Certificado</th>
      <th scope="col">DepartNombre</th> 
      <th scope="col">ProvNombre</th> 
      <th scope="col">DistNombre</th> 
      <th scope="col">A�o Academico</th> 
      <th scope="col">Opciones</th>
    </tr>
  </thead>
  <tbody>
	
	<c:forEach items="${listAlumno}" var="item">
  	<tr>
    	<td scope="row">${item.apellidoPaterno} ${item.apellidoMaterno}</td>
    	<td>${item.nombre}</td>
    	<td>${item.fechaNacimiento}</td>
    	<td>${item.DNI}</td>
    	<td>${item.sexo}</td>
    	<td><img src="${item.foto}" width="30" height="30" alt="img"></td>
    	<td>${item.partidaNacimiento}</td>
    	<td>${item.direccion}</td>
    	<td>${item.estado}</td>
    	<td>${item.fichaMatriculaSiagie}</td>
    	<td>${item.certificadoEstudios}</td>
    	<td>${item.departamentoNombre}</td>
    	<td>${item.provinciaNombre}</td>
    	<td>${item.distritoNombre}</td>
    	<td>${item.consolidadoMatriculaAnioAcademico}</td>
    	<td> 
    		<a href="#" class="btn btn-success"><i class="material-icons">edit</i></a>
    		| 
    		<a href="#" class="btn btn-danger"><i class="material-icons">delete</i></a>
  	</tr>
	</c:forEach>

  </tbody>
</table>

<script>
	function myFunction() {
  		var input, filter, table, tr, td, i;
  		input = document.getElementById("myInput");
  		filter = input.value.toUpperCase();
  		table = document.getElementById("myTable");
  		tr = table.getElementsByTagName("tr");
  	for (i = 0; i < tr.length; i++) {
    	td = tr[i].getElementsByTagName("td")[0];
    		if (td) {
      		if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        		tr[i].style.display = "";
      		} else {
        	tr[i].style.display = "none";
      		}
    	  }       
  		}
	}
</script>

</body>
</html>
