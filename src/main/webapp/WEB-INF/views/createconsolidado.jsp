<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
	<title>Crear Nuevo Consolidado</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
	

</head>
<body>
	<h1>Crear Nuevo Consolidado</h1>

	<form action="saveconsolidado" method="post"> 

  	<div class="form-group">
    	<label for="">A�o Academico</label>
		<input class="date-own form-control" type="text" name="anioAcademico" id="anioAcademico" required>
  	</div>
  	  	
  	<div class="form-group">
    	<label for="">Nivel</label>
    	<select name="nivelId" id="nivelId">
    		<option value=""> Seleccione un Nivel... </option>
    		<c:forEach items="${listNivel}" var="item">
  			<option value="${item.nivelId}">${item.nombre}</option>
  			</c:forEach>
		</select>
  	</div>
  	
  	<div class="form-group">
    	<label for="">Grado</label>
    	<select name="gradoId" id="gradoId">
    		<option value=""> Seleccione un Grado... </option>
    		<c:forEach items="${listGrado}" var="item">
  			<option value="${item.gradoId}">${item.nombre}</option>
  			</c:forEach>
		</select>
  	</div>
	
  	<button type="submit" class="btn btn-primary">Crear</button>
  	
	</form>
</body>

  <script type="text/javascript">
      $('.date-own').datepicker({
         minViewMode: 2,
         format: 'yyyy'
       });
  </script>

</html>