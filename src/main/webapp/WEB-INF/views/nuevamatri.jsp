<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>Alexander Von Humboldt</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.cyan-light_blue.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">  
	  <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script> 
	  <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>   
    <script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
         <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
  </head>
  <body>
    <div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
      <header class="demo-header mdl-layout__header mdl-color--grey-100 mdl-color-text--grey-600">
        <div class="mdl-layout__header-row">
          <span class="mdl-layout-title">Nueva Matricula</span>
          <div class="mdl-layout-spacer"></div>
        </div>
      </header>
      <div class="demo-drawer mdl-layout__drawer mdl-color--blue-grey-900 mdl-color-text--blue-grey-50">
        <header class="demo-drawer-header">
          <img src="https://image.ibb.co/mGn5np/user.jpg" class="demo-avatar">
          
          <div class="demo-avatar-dropdown">
          
          <!-- EMAIL USUARIO -->
            <span>hello@example.com</span>
            <div class="mdl-layout-spacer"></div>
            <button id="accbtn" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">
              <i class="material-icons" role="presentation">arrow_drop_down</i>
              <span class="visuallyhidden">Accounts</span>
            </button>
            <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" for="accbtn">
              <li class="mdl-menu__item">hello@example.com</li>
              <li class="mdl-menu__item">info@example.com</li>         
            </ul>
          </div>
        </header>
        <nav class="demo-navigation mdl-navigation mdl-color--blue-grey-800">
          <a class="mdl-navigation__link" href=""><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Home</a>
          <a class="mdl-navigation__link" href=""><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">inbox</i>Inbox</a>
          <a class="mdl-navigation__link" href=""><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">delete</i>Trash</a>
          <a class="mdl-navigation__link" href=""><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">report</i>Spam</a>
          <a class="mdl-navigation__link" href=""><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">forum</i>Forums</a>
                  
        </nav>
      </div>
      <main class="mdl-layout__content mdl-color--grey-100">
 
<div class="container">
<div id="accordion">
<br>
    <!--SECT 1-->
    <div class="card card-default">
        <div class="card-header">
            <h4 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                    <i class="glyphicon glyphicon-search text-gold"></i>
                    <b>SECCI�N 1: INFORMACI�N DEL ALUMNO</b>
                </a>
            </h4>
        </div>
        <div id="collapse1" class="collapse show">
            <div class="card-body">
            
                <!-- DNI -->
                <div class="row">
                    <div class="col-md-3 col-lg-5">
                        <div class="form-group">
                            <label class="control-label">DNI</label>
                            <input type="text" class="form-control" />
                        </div>
                    </div>                  
                    
                </div>

                <!-- AP PATERNO -->
                <div class="row">
                    <div class="col-md-4 col-lg-8">
                        <div class="form-group">
                            <label class="control-label">Apellido Paterno</label>
                            <input type="text" class="form-control" />
                        </div>
                    </div>
                </div>

                <!-- AP MATERNO -->
                <div class="row">
                    <div class="col-md-4 col-lg-8">
                        <div class="form-group">
                            <label class="control-label">Apellido Materno</label>
                            <input type="text" class="form-control" />
                        </div>
                    </div>
                </div>
                
                <!-- NOMBRE -->
                <div class="row">
                    <div class="col-md-4 col-lg-8">
                        <div class="form-group">
                            <label class="control-label">Nombres</label>
                            <input type="text" class="form-control" />
                        </div>
                    </div>
                </div>
                
                <!-- SEXO/PARTIDA -->
                <div class="row">
                    <div class="col-md-3 col-lg-3">
                        <div class="form-group">
                            

                        
                        
                            <label class="control-label">Sexo</label>                         
                            <div>
                            <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
  <label class="form-check-label" for="inlineRadio1">Hombre</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
  <label class="form-check-label" for="inlineRadio2">Mujer</label>
</div>
                            </div>
                            

                        </div>
                    </div>
                    <div class="col-md-3 col-lg-8">
                        <div class="form-group">
                          <label class="control-label">Partida de Nacimiento</label>
                          <div class="form-group">
                          <input type="file" class="form-control-file" id="exampleFormControlFile1">
                          </div>
                        </div>
                        
                    </div>           
                </div>
                
            </div>`
        </div>
    </div>
    <!--SECT 2-->
    <div class="card card-default">
        <div class="card-header">
            <h4 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                    <i class="glyphicon glyphicon-lock text-gold"></i>
                    <b>SECCI�N 2: DATOS DE NACIMIENTO</b>
                </a>
            </h4>
        </div>
        <div id="collapse2" class="collapse show">
            <div class="card-body">

                <!--FECHA NAC-->
                <div class="row">    
                  <div class="col-md-2 col-lg-5">
                        <div class="form-group">
                            <label class="control-label">Fecha de Nacimiento</label>
                            <div class="input-group date">
                                <input class="form-control" type="text" />
                               <span class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button></span>
                            </div>
                        </div>
                    </div>
                </div>    

                <div class="row">
                  <div class="col-md-2 col-lg-4">
                    <label for="inputState">Departamento</label>
                    <select id="inputState" class="form-control">
                      <option selected>Seleccione un departamento...</option>
                      <c:forEach items="${listDepartamento}" var="item">
  						<option value="${item.departamentoId}">${item.nombre}</option>
  					  </c:forEach>
                    </select>
                  </div>
                  
                  <div class="col-md-2 col-lg-4">
                    <label for="inputState">Provincia</label>
                    <select id="inputState" class="form-control">
                    <option selected>Seleccione un provincia...</option>
                      <c:forEach items="${listProvincia}" var="item">
  						<option value="${item.provinciaId}">${item.nombre}</option>
  					  </c:forEach>
                    </select>
                  </div>
                  <div class="col-md-2 col-lg-4">
                    <label for="inputState">Distrito</label>
                    <select id="inputState" class="form-control">
                    <option selected>Seleccione un distrito...</option>
                      <c:forEach items="${listDistrito}" var="item">
  						<option value="${item.distritoId}">${item.nombre}</option>
  					  </c:forEach>
                    </select>
                  </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-4 col-lg-8">
                        <div class="form-group">
                            <label class="control-label">Direcci�n</label>
                            <input type="text" class="phone form-control" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--SECT 3-->
    <div class="card card-default">
        <div class="card-header">
            <h4 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                    <i class="glyphicon glyphicon-search text-gold"></i>
                    <b>SECCI�N 3: ENTREGABLES</b>
                </a>
            </h4>
        </div>
        <div id="collapse3" class="collapse show">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3 col-lg-5">
                        <div class="form-group">
                          <label class="control-label">Certificado de Estudios</label>
                          <div class="form-group">
                          <input type="file" class="form-control-file" id="exampleFormControlFile1">
                          </div>
                        </div>
                        
                    </div> 
                    
                    <div class="col-md-3 col-lg-5">
                          <div class="form-group">
                          <label class="control-label">Ficha de Matricula SIAGIE</label>
                          <div class="form-group">
                          <input type="file" class="form-control-file" id="exampleFormControlFile1">
                          </div>
                        </div>                  
                    </div>
            </div>
                <div class="row">
                    <div class="col-md-3 col-lg-5">
                        <div class="form-group">
                          <label class="control-label">Fotograf�a</label>
                          <div class="form-group">
                          <input type="file" class="form-control-file" id="exampleFormControlFile1">
                          </div>
                        </div>
                        
                    </div> 
                    
                    <div class="col-md-2 col-lg-4">
                    <label for="inputState">Estado</label>
                    <select id="inputState" class="form-control">
                      <option selected>Seleccione un estado...</option>
                      <option>...</option>
                    </select>
                  </div>
            </div>
            
        </div>
    </div>

</div>
    <!--SECT 4-->
    <div class="card card-default">
        <div class="card-header">
            <h4 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                    <i class="glyphicon glyphicon-search text-gold"></i>
                    <b>SECCI�N 4: APODERADO</b>
                </a>
            </h4>
        </div>
        <div id="collapse4" class="collapse show">
            <div class="card-body">
                <!-- DNI -->
                <div class="row">
                    <div class="col-md-3 col-lg-5">
                        <div class="form-group">
                            <label class="control-label">DNI</label>
                            <input type="text" class="form-control" />
                        </div>
                    </div>                  
                    
                </div>

                <!-- AP PATERNO -->
                <div class="row">
                    <div class="col-md-4 col-lg-8">
                        <div class="form-group">
                            <label class="control-label">Apellido Paterno</label>
                            <input type="text" class="form-control" />
                        </div>
                    </div>
                </div>

                <!-- AP MATERNO -->
                <div class="row">
                    <div class="col-md-4 col-lg-8">
                        <div class="form-group">
                            <label class="control-label">Apellido Materno</label>
                            <input type="text" class="form-control" />
                        </div>
                    </div>
                </div>
                
                <!-- NOMBRE -->
                <div class="row">
                    <div class="col-md-4 col-lg-8">
                        <div class="form-group">
                            <label class="control-label">Nombres</label>
                            <input type="text" class="form-control" />
                        </div>
                    </div>
                </div> 
                
                <!--FECHA NAC-->
                <div class="row">    
                  <div class="col-md-2 col-lg-5">
                        <div class="form-group">
                            <label class="control-label">Fecha de Nacimiento</label>
                            <div class="input-group date">
                                <input class="form-control" type="text" />
                               <span class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button></span>
                            </div>
                        </div>
                    </div>
                </div> 
                
                <!--GRADO INST-->
                <div class="row">
                <div class="col-md-2 col-lg-4">
                    <label for="inputState">Grado de Instrucci�n</label>
                    <select id="inputState" class="form-control">
                      <option selected>Seleccione un grado de instrucci�n...</option>
                      <option>...</option>
                    </select>
                  </div>
                </div>   
                <br>
                <!--TELEFONO-->
                <div class="row">
                    <div class="col-md-4 col-lg-6">
                        <div class="form-group">
                            <label class="control-label">Tel�fono</label>
                            <input type="text" class="form-control" />
                        </div>
                    </div>
                </div>
                
                <!--EMAIL-->
                <div class="row">
                    <div class="col-md-4 col-lg-8">
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input type="email" class="form-control" />
                        </div>
                    </div>
                </div>
                
                <!--DIRECCION-->
                <div class="row">
                    <div class="col-md-4 col-lg-8">
                        <div class="form-group">
                            <label class="control-label">Direcci�n</label>
                            <input type="text" class="phone form-control" />
                        </div>
                    </div>
                </div>
        </div>
    </div>

</div>
    <br />
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="pull-right">
            <a href="#" class="btn btn-success btn-lg" id="btnSubmit"><i class="fa fa-save"></i> Guardar</a>
<button class="btn btn-warning btn-lg" type="reset" value="Reset" id="btnToTop"><i class="fa fa-arrow-up"></i> Reset</button>
        </div>
    </div>
</div>
</div>

</main>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    
    <script>
     $(function () {
        $(".date").datepicker({
            autoclose: true,
            todayHighlight: true
        });
    });
    </script>
  <script>
  var button = document.createElement('button');
  var textNode = document.createTextNode('Click Me!');
  button.appendChild(textNode);
  button.className = 'mdl-button mdl-js-button mdl-js-ripple-effect';
  componentHandler.upgradeElement(button);
  document.getElementById('container').appendChild(button);
  
</script>
</div>
  </body>
</html>