<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
<head>
	<title>Consolidados de Matricula</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>
	<link href="<c:url value="/resources/main.css" />" rel="stylesheet">
	<style>
	#var {
  		float: right;
  		clear: both;
	}
	</style>
</head>
<body>
<hr>
<h1>Consolidados de Matricula</h1>
<div class="alert alert-primary" role="alert">
<button onclick="document.getElementById('id01').style.display='block'" style="width:auto;" id="btncrear">Nuevo Consolidado de Matricula</button>
  <div id="var">Buscar: <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Buscar por consolidado..." title="Ingresa el nombre del consolidado"> </div>
</div>
<br>
<table class="table" id="myTable">
  <thead class="thead-dark">
    <tr>
      <th scope="col">A�o Academico</th>
      <th scope="col">Nivel Nombre</th>
      <th scope="col">Grado Nombre</th>
      <th scope="col">Opciones</th>
    </tr>
  </thead>
  <tbody>
	
	<c:forEach items="${listConsolidadoMatricula}" var="item">
  	<tr>
    	<td scope="row">${item.anioAcademico}</td>
    	<td>${item.nivelNombre}</td>
    	<td>${item.gradoNombre}</td>
    	<td> 
    		<a href="buscarconsolidado/${item.consolidadoMatriculaId}" class="btn btn-success"><i class="material-icons">edit</i></a>
    		| 
    		<a href="deleteconsolidado/${item.consolidadoMatriculaId}" class="btn btn-danger"><i class="material-icons">delete</i></a>
  	</tr>
	</c:forEach>

  </tbody>
</table>


<div id="id01" class="modal">
   <form class="modal-content animate" action="saveconsolidado" method="post">
    <div class="form-group">
    	<label for="">A�o Academico</label>
		<input class="date-own form-control" type="text" name="anioAcademico" id="anioAcademico" required>
  	</div>
  	  	
  	<div class="form-group" style="width:200px;">
    	<label for="">Nivel</label>
    	<select name="nivelId" id="nivelId">
    		<option value=""> Seleccione un Nivel... </option>
    		<c:forEach items="${listNivel}" var="item">
  			<option value="${item.nivelId}">${item.nombre}</option>
  			</c:forEach>
		</select>
  	</div>
  	
  	<div class="form-group" style="width:200px;">
    	<label for="">Grado</label>
    	<select name="gradoId" id="gradoId">
    		<option value=""> Seleccione un Grado... </option>
    		<c:forEach items="${listGrado}" var="item">
  			<option value="${item.gradoId}">${item.nombre}</option>
  			</c:forEach>
		</select>
  	</div>
	
	<div class="form-group" >
  	<button type="submit" id="btncrear">Crear</button>
	</div>
    <div class="form-group" style="background-color:#f1f1f1">
      <button type="submit" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
    </div>
  </form>
</div>


<script>
	function myFunction() {
  		var input, filter, table, tr, td, i;
  		input = document.getElementById("myInput");
  		filter = input.value.toUpperCase();
  		table = document.getElementById("myTable");
  		tr = table.getElementsByTagName("tr");
  	for (i = 0; i < tr.length; i++) {
    	td = tr[i].getElementsByTagName("td")[0];
    		if (td) {
      		if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        		tr[i].style.display = "";
      		} else {
        	tr[i].style.display = "none";
      		}
    	  }       
  		}
	}
</script>

<script type="text/javascript">
      $('.date-own').datepicker({
         minViewMode: 2,
         format: 'yyyy'
       });
  </script>

  <script>
var modal = document.getElementById('id01');
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>
</body>
</html>
