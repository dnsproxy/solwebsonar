<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>Alexander Von Humboldt</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.cyan-light_blue.min.css">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<link href="/resources/index.css" rel="stylesheet" type="text/css"/>
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

	<style type="text/css">
h4 {
  font-family: 'Tw Cen MT', Calibri, 'Century Gothic';
  font-size: 1.17em;
}

p{
	font-family: Calibri, 'Century Gothic';
}
	/* Site Heading */
.site-heading h3{
    font-size : 40px;
    margin-bottom: 10px;
    font-weight: 600;
}
.border {
    background: #e8e8e8 ;
    height: 1px;
    width: 40%;
    margin-left: auto;
    margin-right: auto;
    margin-bottom: 45px;
    float:left;
}

/* Feature-CSS */

.icon {
    color : #fff;
    padding:15px;
    background:#323232 ;
    font-size:50px;
    border-radius:90px;
    border:10px solid #323232 ;
    
}
.feature-box {
    text-align: center;
    padding: 20px;
    transition: .5s;
    margin-bottom: 30px;
    border: 1px solid #e8e8e8;
}
.feature-box:hover {
   
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
.feature-box h4 {
    font-size: 20px;
    font-weight: 600;
    margin: 25px 0 15px;
}

.hide-bullets {
list-style:none;
margin-left: -50px;
margin-top:20px;
}
#btnTraslado{
    background: #009688;
    color: #ffffff;
    border-color: #009688;
}
#hinsignea{
padding: 75px;
}


#letras{
	color: white;
}

#letras:hover {
  background-color: #263238;
}


	</style>
		<link href="/resources/stars.css" rel="stylesheet" type="text/css"/>
	<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
  </head>

  <body>
    <div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
      <header class="demo-header mdl-layout__header mdl-color--grey-100 mdl-color-text--grey-600">
        <div class="mdl-layout__header-row">
          <span class="mdl-layout-title">Bienvenido(a) ${usuario}</span>
          <div class="mdl-layout-spacer"></div>
        </div>
      </header>
      <div class="demo-drawer mdl-layout__drawer mdl-color--blue-grey-900 mdl-color-text--blue-grey-50">
        <header class="demo-drawer-header">
          <img id="hinsignea" src="${pageContext.request.contextPath}/resources/hinsignea.png"/>
          
          <div class="demo-avatar-dropdown">
          
          <!-- EMAIL USUARIO -->
            <div class="mdl-layout-spacer"></div>
          </div>
        </header>
        <nav class="demo-navigation mdl-navigation mdl-color--blue-grey-900">
          <a id="letras"class="mdl-navigation__link" href=""><i class="glyphicon glyphicon-plus" role="presentation" ></i>  Nueva Matricula</a>
          <a id="letras" class="mdl-navigation__link" href=""><i class="glyphicon glyphicon-pencil" role="presentation" ></i>  Ratificar Matricula</a>
          <a id="letras" class="mdl-navigation__link" href=""><i class="glyphicon glyphicon-resize-horizontal" role="presentation" ></i>  Matricula Traslado</a>
          <a id="letras" class="mdl-navigation__link" href=""><i class="glyphicon glyphicon-list" role="presentation" ></i>  Lista Consolidados</a>
          <a id="letras" class="mdl-navigation__link" href=""><i class="glyphicon glyphicon-user" role="presentation" ></i>  Lista Alumnos</a>
		  <a id="letras" class="mdl-navigation__link" href=""><i class="glyphicon glyphicon-cog" role="presentation" ></i>  Configurar Sistema</a>   
        </nav>
      </div>
      <main class="mdl-layout__content mdl-color--grey-100" id="btncont">
<!-- BUTTONS -->
<div class="container" id="btncont">
      <div class="Features-section paddingTB60 ">
    <div class="container">
    <br>
	<!-- <div class="row">
		 <div class="col-md-12 site-heading ">
						<div class="border">

						</div>
					</div>
	</div>-->
	<div class="row">
	  <div class="container">
        <div id="main_area">
                <!-- Slider -->
                <div class="row">
                    <div class="col-xs-12" id="slider">
                        <!-- Top part of the slider -->
                        <div class="row">
                            <div class="col-sm-12" id="carousel-bounding-box">
                                <div class="carousel slide" id="myCarousel">
                                    <!-- Carousel items -->
                                    <div class="carousel-inner">
                                        <div class="active item" data-slide-number="0">
                                         <img id="" src="${pageContext.request.contextPath}/resources/img1.png" />
                                         </div>

                                        <div class="item" data-slide-number="1">
                                        <img id="" src="${pageContext.request.contextPath}/resources/img2.png" />
                                        </div>

                                        <div class="item" data-slide-number="2">
                                        <img id="" src="${pageContext.request.contextPath}/resources/img3.jpg" />
                                        </div>

                                        <div class="item" data-slide-number="3">
                                        <img id="" src="${pageContext.request.contextPath}/resources/img4.png" />
                                        </div>

                                        <div class="item" data-slide-number="4">
                                        <img id="" src="${pageContext.request.contextPath}/resources/img5.png" />
                                        </div>

                                        <div class="item" data-slide-number="5">
                                        <img id="" src="${pageContext.request.contextPath}/resources/img6.png" />
                                        </div>
                                    </div><!-- Carousel nav -->
                                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>                                       
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span>                                       
                                    </a>                                
                                    </div>
                            </div>
                        </div>
                    </div>
                </div><!--/Slider-->
                </br>
                </br>
                </br>
						<div class="col-sm-4 col-md-4">
							<div class="col-md-12 feature-box">
								<img id="" src="${pageContext.request.contextPath}/resources/imgnuevo.png" />
								<h4>Matricula Nueva</h4>
								<p>Disponible para alumnos nuevos en la institucion Educativa.</p>
								<a href="createalumno" class="btn btn-sq-lg btn-success" id="btnTraslado">Matricular</a>

							</div>
						</div> <!-- End Col -->
						<div class="col-sm-4 col-md-4">
								<div class="col-md-12 feature-box">
								<img id="" src="${pageContext.request.contextPath}/resources/imgratificar.png" />
								<h4>Ratificar Matricula</h4>
								<p>Disponible para alumnos integrantes de la institucion Educativa.</p>
							    <a href="createalumno" class="btn btn-sq-lg btn-success" id="btnTraslado">Matricular</a>
							</div>
						</div> <!-- End Col -->	
						<div class="col-sm-4 col-md-4">
								<div class="feature-box">
								<img id="" src="${pageContext.request.contextPath}/resources/imgtraslado.png" />
								<h4>Matricula Traslado</h4>
								<p>Disponible para alumnos trasladados de otra institucion Educativa.</p>
							    <a href="createalumno" class="btn btn-sq-lg btn-success" id="btnTraslado">Matricular</a>
							</div>
						</div> <!-- End Col -->
						<div class="col-sm-4 col-md-4">
								<div class="feature-box">
								<img id="" src="${pageContext.request.contextPath}/resources/imgconsolidado.png" />
								<h4>Consolidados</h4>
								<p>Se Muestra la Lista de consolidados de la Intitucion Educativa.</p>
							    <a href="indexconsolidado" class="btn btn-sq-lg btn-success" id="btnTraslado">Ver Consolidados</a>
							</div>
						</div> <!-- End Col -->
						<div class="col-sm-4 col-md-4">
								<div class="feature-box">
								<img id="" src="${pageContext.request.contextPath}/resources/imglista.png" />
								<h4>Lista de Alumnos</h4>
								<p>Se Muestra la Lista de Alumnos generales de la Institucion Educativa.</p>
							    <a href="indexconsolidado" class="btn btn-sq-lg btn-success" id="btnTraslado">Ver Alumnos</a>
							</div>
						</div> <!-- End Col -->
						<div class="col-sm-4 col-md-4">
								<div class="feature-box">
								<img id="" src="${pageContext.request.contextPath}/resources/imgconfig.png" />
								<h4>Configuracion del Sistema</h4>
								<p>Se muestra la configuracion de usuarios del sistema .</p>
							    <a href="indexconsolidado" class="btn btn-sq-lg btn-success" id="btnTraslado">Configurar Sistema</a>
							</div>
						</div> <!-- End Col -->
					</div>
</div>
</div>
</div>
</div>
</div>
      </main>
    </div>

    <script src="https://code.getmdl.io/1.3.0/material.min.js"></script>

  <script>
  var button = document.createElement('button');
  var textNode = document.createTextNode('Click Me!');
  button.appendChild(textNode);
  button.className = 'mdl-button mdl-js-button mdl-js-ripple-effect';
  componentHandler.upgradeElement(button);
  document.getElementById('container').appendChild(button);
</script>

<script>
jQuery(document).ready(function($) {
	 
    $('#myCarousel').carousel({
            interval: 5000
    });

    $('#carousel-text').html($('#slide-content-0').html());

    //Handles the carousel thumbnails
   $('[id^=carousel-selector-]').click( function(){
        var id = this.id.substr(this.id.lastIndexOf("-") + 1);
        var id = parseInt(id);
        $('#myCarousel').carousel(id);
    });


    // When the carousel slides, auto update the text
    $('#myCarousel').on('slid.bs.carousel', function (e) {
             var id = $('.item.active').data('slide-number');
            $('#carousel-text').html($('#slide-content-'+id).html());
    });
});
</script>
  </body>
</html>
